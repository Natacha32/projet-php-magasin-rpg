/*DROP DATABASE IF EXISTS `rpg_data`;*/
CREATE DATABASE  IF NOT EXISTS `b6_25907522_rpg_data`;
USE `b6_25907522_rpg_data`;

/*CREATION DES TABLES*/

/*Structure table joueur*/
DROP TABLE IF EXISTS JOUEUR;
CREATE TABLE IF NOT EXISTS JOUEUR (
`id_joueur` int(11) NOT NULL AUTO_INCREMENT,
`pseudo` varchar(250) NOT NULL,
`argent` bigint(50) NULL,
PRIMARY KEY (`id_joueur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*Structure table classe*/
DROP TABLE IF EXISTS CLASSE;
CREATE TABLE IF NOT EXISTS CLASSE (
`id_classe` int(11) NOT NULL AUTO_INCREMENT,
`libelle` varchar(20) NOT NULL,
PRIMARY KEY (`id_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*Structure table caracteristique*/
DROP TABLE IF EXISTS CARASTERISTIQUE;
CREATE TABLE IF NOT EXISTS CARACTERISTIQUE (
`id` int(11) NOT NULL AUTO_INCREMENT,
`libelle` varchar(20) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*Structure table type*/
DROP TABLE IF EXISTS TYPE;
CREATE TABLE IF NOT EXISTS TYPE (
`id_type` int(11) NOT NULL AUTO_INCREMENT,
`libelle` varchar(20) NOT NULL,
`id_classe` int(11) NOT NULL,
PRIMARY KEY (`id_type`),
FOREIGN KEY (`id_classe`) REFERENCES CLASSE(`id_classe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*Structure table objet*/
DROP TABLE IF EXISTS OBJET;
CREATE TABLE IF NOT EXISTS OBJET (
`id_objet` int(11) NOT NULL AUTO_INCREMENT,
`libelle` varchar(250) NOT NULL,
`prix` int(11) NOT NULL, /*A COMPLETER*/
`id_type` int(11) NOT NULL,
PRIMARY KEY (`id_objet`),
FOREIGN KEY (`id_type`) REFERENCES TYPE(`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*Structure table modifier*/
DROP TABLE IF EXISTS MODIFIER;
CREATE TABLE IF NOT EXISTS MODIFIER (
`id_objet` int(11) NOT NULL,
`id_carac` int(11) NOT NULL,
`valeur` int(20) NOT NULL,
PRIMARY KEY (`id_objet`, `id_carac`),
FOREIGN KEY (`id_carac`) REFERENCES CARACTERISTIQUE(`id`),
FOREIGN KEY (`id_objet`) REFERENCES OBJET(`id_objet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*Structure table posseder*/
DROP TABLE IF EXISTS POSSEDER;
CREATE TABLE IF NOT EXISTS POSSEDER (
`id_joueur` int(11) NOT NULL,
`id_objet` int(11) NOT NULL,
`qte` int(20) NOT NULL,
PRIMARY KEY (`id_joueur`, `id_objet`),
FOREIGN KEY (`id_objet`) REFERENCES OBJET(`id_objet`),
FOREIGN KEY (`id_joueur`) REFERENCES JOUEUR(`id_joueur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

/*INSERTION DE DONNEES DANS LES TABLES*/
INSERT INTO JOUEUR (`id_joueur`, `pseudo`, `argent`) VALUES
(1, 'Zadrudula', 596),
(2, 'Lovius', 999),
(3, 'Glazeblood', 30),
(4, 'Wulflake', 210),
(5, 'Mighteagle', 150);

INSERT INTO CLASSE (`id_classe`, `libelle`) VALUES
(1, 'arme'),
(2, 'consommable'),
(3, 'armure'),
(4, 'accessoire');

INSERT INTO TYPE (`id_type`, `libelle`, `id_classe`) VALUES
(1, 'gemme', 4),
(2, 'bijou', 4),
(3, 'parchemin', 1),
(4, 'baguette', 1),
(5, 'épée', 1),
(6, 'fléau', 1),
(7, 'hache', 1),
(8, 'lance', 1),
(9, 'arc', 1),
(10, 'armure', 3),
(11, 'vêtement', 3),
(12, 'nourriture', 2),
(13, 'potion', 2);

INSERT INTO OBJET (`id_objet`, `libelle`, `prix`, `id_type`) VALUES

(1, 'Gemme du Feu', 500, 1),
(2, "Gemme de l'Eau", 500, 1),
(3, "Gemme de la Lumière", 500, 1),
(4, 'Gemme des Ténèbres', 500, 1),
(5, 'Gemme du Sang', 500, 1),
(6, 'Gemme de la Nature', 500, 1),
(7, 'Anneau du Serment', 800, 2),
(8, 'Pendentif du Silence', 1000, 2),
(9, 'Orbe du Phénix', 1500, 1),
(10, "Grimoire du Magicien d'Oz", 1500, 3),
(11, 'Baguette basique', 500, 4),
(12, "Sort d'éternuement", 50, 3),
(13, 'Sceptre du Cataclysme', 1500, 4),
(14, 'Pourfendeur de Démons', 3000, 5),
(15, 'Dague Antique', 750, 5),
(16, 'Épée basique', 300, 5),
(17, 'Fléau de la Dépravation', 650, 6),
(18, 'Hache du Berserker', 1200, 7),
(19, "Arc d'Artémis", 950, 9),
(20, 'Lance basique', 500, 8),
(21, 'Blouson en cuir', 500, 11),
(22, 'Cape de mage', 1500, 11),
(23, 'Chemise de villageois', 150, 11),
(24, 'Cotte de mailles', 700, 10),
(25, 'Habit de moine', 700, 11),
(26, "Tunique d'aventurier", 650, 11),
(27, "Armure de plates", 2000, 10),
(28, "Baie bleue", 5, 12),
(29, "Baie jaune", 5, 12),
(30, "Baie indigo", 8, 12),
(31, "Baie rouge", 8, 12),
(32, "Potion de soin basique", 10, 13),
(33, "Potion de magie basique", 10, 13),
(34, "Potion de soin intermédiaire", 50, 13),
(35, "Potion de magie intermédiaire", 50, 13),
(36, "Potion de soin avancée", 100, 13),
(37, "Potion de magie avancée", 100, 13);

INSERT INTO CARACTERISTIQUE (`id`, `libelle`) VALUES
(1, 'Attaque'),
(2, 'Défense'),
(3, 'PV'),
(4, 'Mana'),
(5, 'Vitesse');

INSERT INTO MODIFIER (`id_objet`, `id_carac`, `valeur`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 2, 1),
(3, 5, 1),
(4, 5, 1),
(4, 1, 1),
(5, 2, 1),
(5, 1, 1),
(6, 5, 1),
(6, 2, 1),
(7, 2, 3),
(8, 5, 3),
(8, 2, 1),
(9, 1, 3),
(9, 5, 2),
(9, 2, 1),
(10, 1, 20),
(11, 1, 5),
(12, 1, 1),
(13, 1, 15),
(13, 5, 5),
(14, 1, 50),
(15, 1, 10),
(15, 5, 10),
(16, 1, 5),
(17, 1, 15),
(18, 1, 25),
(19, 1, 15),
(19, 5, 5),
(20, 1, 8),
(21, 2, 10),
(22, 2, 10),
(22, 5, 10),
(23, 2, 5),
(24, 2, 20),
(25, 2, 15),
(25, 5, 5),
(26, 2, 15),
(27, 2, 35),
(28, 4, 5),
(29, 4, 8),
(30, 3, 5),
(31, 3, 8),
(32, 3, 10),
(33, 4, 10),
(34, 3, 15),
(35, 4, 15),
(36, 3, 20),
(37, 4, 20);

INSERT INTO POSSEDER (`id_joueur`, `id_objet`, `qte`) VALUES
(1, 10, 3),
(3, 4, 23),
(3, 5, 50),
(4, 5, 20),
(3, 6, 10),
(4, 1, 3),
(2, 10, 1);

/* MODIFICATIONS */

/* CHANGER : objets, types, caract 
SUPPRIMER : tables COMMANDER + INCLURE */
