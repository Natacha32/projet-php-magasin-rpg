# Projet PHP - Magasin RPG : IMARKET STORE

Projet PHP par Kéziah VALORIS, Amélie CROUIGNEAU, Achraf LEMGHARI et Natacha LIAO

Magasin RPG est un magasin en ligne de produits nécessaires à tous joueurs.
Nous accueillons des communautés des quatres coins du monde. Ces joueurs proviennent de tous les univers et de toutes les guildes.
IMARKET Store est l'endroit de toutes les communautés.

![alt text](https://gitlab.com/Natacha32/projet-php-magasin-rpg/-/raw/master/images/connexion.PNG "Page de connexion")

Lorsque vous arrivez sur le site, vous devez vous connecter. Cependant n'ayez crainte jeune padawan, si vous n'avez pas de compte vous pouvez en créer un en cliquant sur se connecter.

Par la suite, vous entrerez au coeur d'un monde plein de surprise et nécessaire à votre survie dans le monde virtuel : la boutique

![alt text](https://gitlab.com/Natacha32/projet-php-magasin-rpg/-/raw/master/images/acces_boutique.PNG "Accès boutique")

Vous avez le choix entre plusieurs catégories : Armes, Armures, Accessoires et Consommables. Vous pouvez choisir d'acquérir chacun d'entre eux... Vous pouvez affiner votre recherche avec des filtres.

![alt text](https://gitlab.com/Natacha32/projet-php-magasin-rpg/-/raw/master/images/armure.PNG "Accès boutique")

Attention cependant à votre budget. Ce dernier est initialisé à 100 lors de votre première connexion. Vous pouvez le voir sur la gauche.  Vous pouvez également vous déconnecter à tout moment.

![alt text](https://gitlab.com/Natacha32/projet-php-magasin-rpg/-/raw/master/images/inventaire.PNG "Inventaire")

Vous pouvez également accéder à votre inventaire en cliquant en bas à droite. En recliquant sur ce même bouton vous pourrez également revenir à la boutique.

Cette application web permet de voir les produits de son inventaire. Il permet également d'en ajouter de nouveaux, d'en supprimer mais mais également de mettre à jour la quantité de ceux que vous possédez déjà.

Il arrive parfois, lorsqu'on achète un objet ou qu'on en jète un, que les changements ne s'opèrent pas à l'écran, il faut alors rafraîchir la page.

Sur ce, bon jeu à vous et puisse le sort vous être favorable...
