<?php
    // Pour charger automatiquement les classes requises
    spl_autoload_register(function ($className) { @include "$className.php"; });
    
        $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php
        $param['id_classe'] =$_POST['category']; //récupérer l'id via ajax

        $stmt = $bd->prepare("SELECT OBJET.libelle as nom, prix, TYPE.libelle as type, 
                                ATTAQUE.valeur as attaque, DEFENSE.valeur as defense, VITESSE.valeur as vitesse,
                                PV.valeur as pv, MANA.valeur as mana
                                FROM (OBJET join TYPE using (id_type)) join CLASSE using(id_classe) 
                                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                                    WHERE CARACTERISTIQUE.libelle = 'attaque') AS ATTAQUE ON ATTAQUE.id_objet = OBJET.id_objet
                                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                                    WHERE CARACTERISTIQUE.libelle = 'defense') AS DEFENSE ON DEFENSE.id_objet = OBJET.id_objet
                                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                                    WHERE CARACTERISTIQUE.libelle = 'vitesse') AS VITESSE ON VITESSE.id_objet = OBJET.id_objet
                                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                                    WHERE CARACTERISTIQUE.libelle = 'pv') AS PV ON PV.id_objet = OBJET.id_objet
                                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                                    WHERE CARACTERISTIQUE.libelle = 'mana') AS MANA ON MANA.id_objet = OBJET.id_objet
                                WHERE id_classe=:id_classe");


        $stmt->execute($param);
        $row=$stmt->fetchAll();

        echo json_encode($row);
?>