<?php

spl_autoload_register(function ($className) { @include "$className.php"; });
$bd = Database::getInstance();

$category = $_POST['category'];
$caract = $bd->query("SELECT id_carac, CARACTERISTIQUE.libelle as caract, MAX(valeur) as max, MIN(valeur) as min FROM MODIFIER 
JOIN OBJET ON MODIFIER.id_objet = OBJET.id_objet
JOIN TYPE ON TYPE.id_type = OBJET.id_type
JOIN CARACTERISTIQUE ON CARACTERISTIQUE.id = MODIFIER.id_carac
WHERE id_classe = $category
GROUP BY id_carac");

echo json_encode($caract->fetchAll());

?>