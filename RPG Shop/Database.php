<?php
// Classe de connexion à une base de données
// S'inspire du pattern singleton pour n'ouvrir qu'une seule connexion
// Utilisation :
//    $bd = Database::getInstance(); // $bd est un objet PDO
class Database {

   // Paramètres pour l'accès à la base
   static private $host = "localhost";
   static private $base = "rpg_data";
   static private $user = "root";
   static private $password = "root";

   static private $pdo = null; // Le singleton

   // Obenir le singleton
   static function getInstance() {
      if (self::$pdo == null) {
         $dsn = sprintf("mysql:host=%s;dbname=%s", self::$host, self::$base);
         try {
            self::$pdo = new PDO($dsn, self::$user, self::$password,
                                 array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
         } catch (PDOException $e) {
            exit('<p class="erreur">Erreur de connexion au serveur '.self::$host.' ('.self::$user
                 .')<br/>'.$e->getMessage().'</p>');
         }
      }
      return self::$pdo;
   }
}

?>
