<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>Boutique RPG</title>
    <link rel="stylesheet" type="text/css" href="style.css" />

</head>

<body>
    <div id="session"><?= session_start(); ?></div>
    <div class="player_info">
        <h2 class="pseudo"><?= $_SESSION['login'] ?></h2> 
        <p class="money"><?= $_SESSION['argent'] ?> pièces d'or</p> 
        <a href="deconnexion.php">Deconnexion</a>
    </div>

    <div class="middle" id="shop">
        <h1>Boutique</h1>

        <div class="filters">
            <p>
            </p>
        </div>

        <div class="categories">
            <h2 id="weapons">Armes</h2>
            <h2 id="armors">Armures</h2>
            <h2 id="accessories">Accessoires</h2>
            <h2 id="consum">Consommables</h2>
        </div>

        <div class="content" id="content-shop">
        </div>
    </div>

    <!--display: none AU DÉBUT-->
    <div class="middle" id="inventory">
        <h1>Inventaire</h2>
        <div class="content" id="content-invent">
        </div>
    </div>

    <input class="switch"  type="button" value="Inventaire">

<script language="javascript" type="text/javascript" src="fct_js.js"></script>

</body>

</html>
