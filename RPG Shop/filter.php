<?php

spl_autoload_register(function ($className) { @include "$className.php"; });
$bd = Database::getInstance();

// INITIALIATION
$carac = $_POST;
unset($carac['type-select']);
$columns = "OBJET.libelle AS nom, OBJET.prix, TYPE.libelle AS type ";
$tables = "LEFT JOIN TYPE ON TYPE.id_type = OBJET.id_type ";
$conditions = "";

// BUILD THE REQUEST
$allcarac = $bd->query("SELECT libelle FROM CARACTERISTIQUE");
foreach($allcarac as $element) {
    $name = strtolower(str_replace(array("é", "è"), "e", $element[0])); // REPLACE é AND è BY e
    $table_name = strtoupper($name); 
    $columns .= ", $table_name.valeur AS $name";
    $tables .= "LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
    WHERE CARACTERISTIQUE.libelle = '$name') AS $table_name ON OBJET.id_objet = $table_name.id_objet ";
}

if($_POST['type-select']!='aucun') { 
    $type = $_POST['type-select'];
    $conditions .= " WHERE TYPE.libelle = '$type'";
}
else {
    $classe = $_POST['classe'];
    $conditions .= " WHERE TYPE.id_classe = '$classe'";
}
foreach(array_keys($carac) as $element) {
    $needle = strstr($element, '-min', true);
    if($needle!=false) {
        $min = $needle . '-min';
        $max = $needle . '-max';
        $c = strtoupper(str_replace(array("é", "è"), "e", $needle)) . ".valeur BETWEEN $carac[$min] AND $carac[$max]";

        if($carac[$min] == 0) {
            $c .= " OR " . strtoupper(str_replace(array("é", "è"), "e", $needle)) . ".valeur IS NULL";
        }

        $conditions .= " AND ($c)";
    }
}

$request = $bd->query("SELECT $columns FROM OBJET $tables $conditions");

echo json_encode($request->fetchAll());