<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Boutique RPG</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body class="connexion">
  <div>
    <h2>Veuillez procéder à votre inscription</h2>
    <form method="POST" action="inscription.php">
      Pseudo : <input type="text" name="login"/>
      <input type="submit" name="Créer"/>
    </form>
    <a href="index.php">Se connecter</a>
  </div>
</body>
</html>

<?php
  include 'JoueurController.php';
  if($_POST){
    if(!empty($_POST['login'])){
      $login = $_POST['login'];
      $joueur = new Joueur($login);
      $joueur->inscription($login);
    }
}
?>