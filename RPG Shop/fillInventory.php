<?php

spl_autoload_register(function ($className) { @include "$className.php"; });
$bd = Database::getInstance();

session_start();
$login=$_SESSION['login'];

$request = $bd->query("SELECT OBJET.libelle as nom, prix, POSSEDER.qte, TYPE.libelle as type, CLASSE.libelle as classe, 
                ATTAQUE.valeur as attaque, DEFENSE.valeur as defense, VITESSE.valeur as vitesse,
                PV.valeur as pv, MANA.valeur as mana
                FROM OBJET
                JOIN TYPE USING (id_type)
                JOIN POSSEDER USING (id_objet)
                JOIN JOUEUR USING (id_joueur)
                JOIN CLASSE USING (id_classe)
                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                    WHERE CARACTERISTIQUE.libelle = 'attaque') AS ATTAQUE ON ATTAQUE.id_objet = OBJET.id_objet
                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                    WHERE CARACTERISTIQUE.libelle = 'defense') AS DEFENSE ON DEFENSE.id_objet = OBJET.id_objet
                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                    WHERE CARACTERISTIQUE.libelle = 'vitesse') AS VITESSE ON VITESSE.id_objet = OBJET.id_objet
                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                    WHERE CARACTERISTIQUE.libelle = 'pv') AS PV ON PV.id_objet = OBJET.id_objet
                LEFT JOIN (SELECT id_objet, CARACTERISTIQUE.libelle, valeur FROM MODIFIER
                    JOIN CARACTERISTIQUE ON MODIFIER.id_carac = CARACTERISTIQUE.id
                    WHERE CARACTERISTIQUE.libelle = 'mana') AS MANA ON MANA.id_objet = OBJET.id_objet
                WHERE JOUEUR.pseudo = '$login'");

//echo $login;
echo json_encode($request->fetchAll());

?>