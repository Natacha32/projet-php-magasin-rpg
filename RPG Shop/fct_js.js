let button = document.querySelector('.switch');
const shop = document.querySelector('#shop');
const inventory = document.querySelector('#inventory');

// SWITCH BETWEEN SHOP AND INVENTORY WHEN CLICKING ON THE BUTTON

button.onclick = () => {
    if(shop.style.display == "none") {
        shop.style.display = "block";
        inventory.style.display = "none";
        button.value = "Inventaire";
    }
    else {
        shop.style.display = "none";
        inventory.style.display = "block";
        button.value = "Boutique";
    }
}

const weapons = document.querySelector('#weapons');
const armors = document.querySelector('#armors');
const accessories = document.querySelector('#accessories');
const consum = document.querySelector('#consum');

// FILLS THE CATEGORY WITH THE CORRESPONDING ITEMS FETCHED FROM THE DB

function fillCategory(category) {

    const form = new FormData();
    form.append('category', category);

    fetch( 'display.php', {
        method: "POST",
        body: form
    })
    .then(response => response.json())
    .then(data => {

        // ARRANGES THE CONTENT THAT WILL BE DISPLAYED IN CLASS = "CONTENT"
        let content_shop = document.querySelector('#shop > .content');
        let text = "";
        let c = "";
        if(category == 1) { c="armes";}
        else if(category == 2) { c="consommables";}
        else if(category == 3) { c="armures";}
        else if(category == 4) { c="accessoires";}

        for( let i=0 ; i<data.length ; i++ ) {
            let img_src = "img/objets/"+c+"/"+encodeURI(data[i]['nom'])+".png";
            text += "<div id=div"+parseInt(i,10)+ " > <img src = " + img_src + "> <p class = 'nom' id='nom"+parseInt(i,10)+"'> " + data[i]["nom"]
            + "</p><p class='prix' id='prix"+i+"'>" + data[i]["prix"] + " pièces d'or</p> <div class ='carac'>";
            if(data[i]['attaque']!=null) { text += "<p>ATK " + data[i]["attaque"] + "</p>";}
            if(data[i]['defense']!=null) { text += "<p>DEF " + data[i]["defense"] + "</p>";}
            if(data[i]['vitesse']!=null) { text += "<p>VIT " + data[i]["vitesse"] + "</p>";}
            if(data[i]['pv']!=null) { text += "<p>PV " + data[i]["pv"] + "</p>";}
            if(data[i]['mana']!=null) { text += "<p>MANA " + data[i]["mana"] + "</p>";}
            text += "</div><input class='acheter' id='button"+i+ "'type='button' value='ACHETER' onclick='addObject(prix"+parseInt(i,10)+", nom"+parseInt(i,10)+")'> </div>";
        }

        content_shop.innerHTML = text;

        let acheter_buttons = document.querySelectorAll('.acheter');
        acheter_buttons.forEach(element => element.addEventListener('click', () => {
            fillInventory();
            readMoney();
        }))
    })
    .catch(error => { console.log(error) });
}

// FILLS THE FILTERS AND CREATE A BUTTON LINKED TO THE FUNCTION FILTER

function fillFilters(category) {

    // ERASE THE CONTENT OF .filters > p

    let filters = document.querySelector('.filters > p');
    filters.innerHTML = "";

    const form = new FormData();
    form.append('category', category);

    // FILL TYPE FILTER

    fetch( 'fill_type.php', {
        method: "POST",
        body: form
    })
    .then(response => response.json())
    .then(data => {

        let filters = document.querySelector('.filters > p');
        let text = "Type <select name='types' id='type-select'> <option value='aucun'>aucun</option> ";

        data.forEach(type => {
            text += "<option value=" + type + ">" + type + "</option>";
        });

        text += "</select> ";
        filters.insertAdjacentHTML('afterbegin', text);
    })
    .catch(error => { console.log(error) });

    // FILL TYPE CARACTERISTIC FILTERS WITH NUMBERS RANGING FROM MIN TO MAX

    fetch( 'fill_caract.php', {
        method: "POST",
        body: form
    })
    .then(response => response.json())
    .then(data => {

        let filters = document.querySelector('.filters > p');
        let text = "";
        let seclected = "";
        //console.log(data);

        data.forEach(caract => {

            text += " " + caract['caract'] +
            " <select class='min' name=" + caract['caract'] + "-min id=" + caract['caract'] + "-min>";

            for(let i = 0; i <= parseInt(caract['max'], 10); i++) {
                text += "<option value=" + i + ">" + i + "</option>";
            }

            text += "</select> - <select class='max' name="+ caract['caract'] + "-max id=" + caract['caract'] + "-max>"
            text += "<p>et</p>";

            for(let i = 0; i <= parseInt(caract['max'], 10); i++) {
                if(i==parseInt(caract['max'], 10)) { seclected = "selected"; };
                text += "<option value=" + i + " " + seclected + ">" + i + "</option>";
            }

            text += "</select>";
        });
        filters.insertAdjacentHTML('beforeend', text);

        // CREATE BUTTON LINKED TO THE FUNCTION FILTER
        filters.insertAdjacentHTML('beforeend', " <input id='filter_button' type='button' value='VALIDER'>");
        document.getElementById('filter_button').addEventListener('click', (event) => {
            event.preventDefault();
            filter(category);
        });

    })
    .catch(error => { console.log(error) });
}


// WHEN CLICKING ON THE CATEGORIES, ACTIVATE FUNCTIONS WITHOUT RELOADING THE PAGE
// ABOUT THE CATGEGORIES : 1 -> "armes", 2 -> "consommations", 3 -> "armures", 4 -> "accessoires"

weapons.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('1');
    fillFilters('1');
    clickedCategory(weapons);
});
armors.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('3');
    fillFilters('3');
    clickedCategory(armors);
});
accessories.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('4');
    fillFilters('4');
    clickedCategory(accessories);
});
consum.addEventListener('click', (event) => {
    event.preventDefault();
    fillCategory('2');
    fillFilters('2');
    clickedCategory(consum);
});

// ADD CLASS clicked ON THE ACTIVE CLICKED CATEGORY

function clickedCategory(category) {
    weapons.classList.remove('clicked');
    consum.classList.remove('clicked');
    accessories.classList.remove('clicked');
    armors.classList.remove('clicked');
    category.classList.add('clicked');
}

// FILTER OBJECTS DISPLAYED

function filter(category) {

    let select = document.querySelectorAll('.filters select');
    let clicked = document.querySelector('[class~=clicked]');

    const form = new FormData();

    if(clicked.id == "weapons") {
        form.append('classe', 1);
    }
    if(clicked.id == "armors") {
        form.append('classe', 3);
    }
    if(clicked.id == "consum") {
        form.append('classe', 2);
    }
    if(clicked.id == "accessories") {
        form.append('classe', 4);
    }

    select.forEach(element => {
        form.append(element.id, element.value);
    });

    fetch('filter.php', {
        method: "POST",
        body: form
    })
    .then(response => response.json())
    .then(data => {
        // INCOMPLETE
        // ARRANGES THE CONTENT THAT WILL BE DISPLAYED IN CLASS = "CONTENT" ACCORDING TO FILTERS
        let content_shop = document.querySelector('#shop > .content');
        let text = "";

        let c = "";
        if(category == 1) { c="armes";}
        else if(category == 2) { c="consommables";}
        else if(category == 3) { c="armures";}
        else if(category == 4) { c="accessoires";}
        for( let i=0 ; i<data.length ; i++ ) {
            let img_src = "img/objets/"+c+"/"+encodeURI(data[i]['nom'])+".png";
            text += "<div id=div"+parseInt(i,10)+ " > <img src = " + img_src + "> <p class = 'nom' id='nom"+i+"'> " + data[i]["nom"]
            + "</p><p class='prix' id='prix"+i+"'>" + data[i]["prix"] + " pièces d'or</p> <div class ='carac'>";
            if(data[i]['attaque']!=null) { text += "<p>ATK " + data[i]["attaque"] + "</p>";}
            if(data[i]['defense']!=null) { text += "<p>DEF " + data[i]["defense"] + "</p>";}
            if(data[i]['vitesse']!=null) { text += "<p>VIT " + data[i]["vitesse"] + "</p>";}
            if(data[i]['pv']!=null) { text += "<p>PV " + data[i]["pv"] + "</p>";}
            if(data[i]['mana']!=null) { text += "<p>MANA " + data[i]["mana"] + "</p>";}
            text += "</div><input class='acheter' id='button"+i+ "'type='button' value='ACHETER' onclick='addObject(prix"+parseInt(i,10)+", nom"+parseInt(i,10)+")'> </div>";
        }

        content_shop.innerHTML = text;

        let acheter_buttons = document.querySelectorAll('.acheter');
        acheter_buttons.forEach(element => element.addEventListener('click', () => {
            fillInventory();
            readMoney();
        }))


    })
    .catch(error => { console.log(error) });
}

function fillInventory() {

    fetch( 'fillInventory.php')
    .then(response => response.json())
    .then(data => {

        // ARRANGES THE CONTENT THAT WILL BE DISPLAYED IN CLASS = "CONTENT"
        let content_invent = document.querySelector('#inventory > .content');
        let text = "";

        for( let i=0 ; i<data.length ; i++) {

            let img_src = "img/objets/" + data[i]["classe"] + "s/"+encodeURI(data[i]['nom'])+".png";
            text += "<div id="+i+"> <img src = " + img_src + "> <p class = 'nom' id='poss"+i+"'>" + data[i]["nom"]
            + "</p> <p class='qte'>QTE "+data[i]['qte']+"</p> <div class ='carac'>";
            if(data[i]['attaque']!=null) { text += "<p>ATK " + data[i]["attaque"] + "</p>";}
            if(data[i]['defense']!=null) { text += "<p>DEF " + data[i]["defense"] + "</p>";}
            if(data[i]['vitesse']!=null) { text += "<p>VIT " + data[i]["vitesse"] + "</p>";}
            if(data[i]['pv']!=null) { text += "<p>PV " + data[i]["pv"] + "</p>";}
            if(data[i]['mana']!=null) { text += "<p>MANA " + data[i]["mana"] + "</p>";}
            text += "</div><input class='jeter' type='button' value='JETER' onclick='deleteObject(poss"+i+")'> </div>";
        }

        content_invent.innerHTML = text;

        let delete_buttons = document.querySelectorAll('.jeter');
        delete_buttons.forEach(element => element.addEventListener('click', () => {
            fillInventory();
        }))
    })
    .catch(error => { console.log(error) });
}
button.addEventListener('click', fillInventory());

function addObject(prixId, nomId){
  prixId= prixId.innerHTML;
  nomId= nomId.innerHTML;
  const form = new FormData();
  form.append('nom', nomId);
  form.append('prix', prixId);
  fetch('addObject.php', {
    method: 'POST',
    body: form
  })
  .then(response => response.json())
  .then(data => {
      fillInventory();
  })
  .catch(error => { console.log(error) });

}

function deleteObject(idNom){
  idNom= idNom.innerHTML;
  const form = new FormData();
  form.append('nom', idNom);
  fetch('deleteObject.php', {
    method: 'POST',
    body: form
  })
  .then(response => response.json())
  .then(data => {
      fillInventory();
  })
  .catch(error => { console.log(error) });

}

function readMoney(){

    fetch('readMoney.php', {
      method: 'POST'
    })
    .then(response => response.json())
    .then(data => {
        document.querySelector('.money').innerHTML = data + " pièces d'or";
    })
    .catch(error => { console.log(error) });
  
}