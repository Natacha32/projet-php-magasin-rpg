<?php



  class Joueur
  {

    private $pseudo;
    private $argent;

    function __construct($pseudo){
      $this->pseudo = $pseudo;
      $this->argent = 0;
    }

    public function lireArgent($pseudo){
      if($_SESSION){
        spl_autoload_register(function ($className) { @include "$className.php"; });
        $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php

        $stmt = $bd->prepare("SELECT argent FROM JOUEUR WHERE pseudo=:login");
        $stmt->bindParam(':login', $pseudo);
        $stmt->execute();
        $row=$stmt->fetchAll();
        $_SESSION['argent'] = $row[0]['argent'];
      }
    }

    public function connexion($pseudo){
      if(!empty($pseudo)){
        if(!is_numeric($pseudo)){
          spl_autoload_register(function ($className) { @include "$className.php"; });
          $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php

          $stmt = $bd->prepare("SELECT pseudo FROM JOUEUR WHERE pseudo=:login");
          $stmt->bindParam(':login', $pseudo);
          $stmt->execute();
          $row=$stmt->fetchAll();
          if(!empty($row)){
            if($row[0]['pseudo'] !== $pseudo)
            {
              $error_message = 'Mauvais login !';
            }
              else
            {
              // On ouvre la session
              session_start();
              // On enregistre le login en session
              $_SESSION['login'] = $pseudo;
              $this->lireArgent($pseudo);
              header('Location: boutique.php');
              exit();
            }
          }
        }
        else{
          $error_message = "Votre pseudo peut-être composé de nombres et de chiffres mais pas uniquement de chiffres.";
        }
      }
      else{
        $error_message = "Veuillez saisir un pseudo.";
      }
    }

    public function deconnexion($pseudo){
        $_SESSION = array();
        session_destroy();
        unset($_SESSION);
        header('Location: index.php');
    }

    public function inscription($pseudo){

      if(!empty($pseudo)){

        if(!is_numeric($pseudo)){
          spl_autoload_register(function ($className) { @include "$className.php"; });
          $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php

          $stmt = $bd->prepare("SELECT pseudo FROM JOUEUR WHERE pseudo=:login");
          $stmt->bindParam(':login', $pseudo);
          $stmt->execute();
          $row=$stmt->fetchAll();

          if(!empty($row))
          {
            $error_message = 'Ce login a déjà été pris. Réessayez avec un autre ou Connectez-vous.';
          }
            else
          {
            $somme_init = 1000;
            $stmt = $bd->prepare("INSERT INTO JOUEUR (pseudo, argent) VALUES (:login, :argent)");
            $stmt->bindParam(':login', $pseudo);
            //$stmt->bindParam(':argent', $this->argent);
            $stmt->bindParam(':argent', $somme_init);
            $stmt->execute();
            // On ouvre la session
            session_start();
            // On enregistre le login en session
            $_SESSION['login'] = $pseudo;
            $this->lireArgent($pseudo);
            header('Location: boutique.php');
            exit();
          }


        }
        else{
          $error_message = "Votre pseudo peut-être composé de nombres et de chiffres mais pas uniquement de chiffres.";
        }
      }
      else{
        $error_message; "Veuillez saisir un pseudo.";
      }
    }

    public function recupId($pseudo){
      spl_autoload_register(function ($className) { @include "$className.php"; });
      $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php
      $stmt = $bd->prepare("SELECT id_joueur FROM JOUEUR WHERE pseudo=:login");
      $stmt->bindParam(':login', $pseudo);
      $stmt->execute();
      $row=$stmt->fetchAll();
      $id = $row[0]['id_joueur'];
      return $id;
    }

    public function recupIdObjet($nom){
      spl_autoload_register(function ($className) { @include "$className.php"; });
      $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php
      $stmt = $bd->prepare("SELECT id_objet FROM OBJET WHERE libelle=:libelle");
      $stmt->bindParam(':libelle', $nom);
      $stmt->execute();
      $row=$stmt->fetchAll();
      return $row[0]['id_objet'];
    }

    public function recupObjet($id, $id_objet){
      spl_autoload_register(function ($className) { @include "$className.php"; });
      $bd = Database::getInstance(); //paramètres connexion à la bd dans le fichier Database.php
      $stmt = $bd->prepare("SELECT OBJET.id_objet, qte FROM OBJET JOIN POSSEDER ON OBJET.id_objet=POSSEDER.id_objet WHERE id_joueur=:id AND OBJET.id_objet=:id_objet");
      if(!empty($id_objet)){
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':id_objet', $id_objet);
        $stmt->execute();
        $row=$stmt->fetchAll();
        $objets[]=$row;
        return $row;
      }

    }

    public function ajoutInventaire($pseudo, $nom, $prix){
      $argent= $_SESSION['argent'];
      //VERIFIER SI L'OBJET EXISTE
      if($prix <= $argent){
        spl_autoload_register(function ($className) { @include "$className.php"; });
        $bd = Database::getInstance();
         //paramètres connexion à la bd dans le fichier Database.php
        $id = $this->recupId($pseudo);
        $id_objet= $this->recupIdObjet($nom);
        if(!empty($id_objet)){
          $objet_id= $id_objet;
        }
        $objet = $this->recupObjet($id, $id_objet);
        if(!empty($objet)){
          $qte = $objet[0]['qte'] + 1;
          $stmt = $bd->prepare("UPDATE POSSEDER SET qte=:qte WHERE id_joueur=:id_joueur AND id_objet=:id_objet");
        }
        else {
          $qte = 1;
          $stmt = $bd->prepare("INSERT INTO POSSEDER (id_joueur, id_objet, qte) VALUES (:id_joueur, :id_objet, :qte)");
        }
        $stmt->bindParam(':qte', $qte);
        $stmt->bindParam(':id_joueur', $id);
        $stmt->bindParam(':id_objet', $objet_id);
        $stmt->execute();
        $this->achat($prix);
        echo json_encode($stmt->fetchAll());
      }
      else{
        $error_message= "Vous n'avez pas assez d'argent.";
        echo json_encode($error_message);
      }
    }

    public function achat($prix){
      $argent = $_SESSION['argent'];
      $argent -=$prix;
      $_SESSION['argent'] = $argent;
      $login=$_SESSION['login'];
      spl_autoload_register(function ($className) { @include "$className.php"; });
      $bd = Database::getInstance();
      $stmt = $bd->prepare("UPDATE JOUEUR SET argent=:argent WHERE pseudo=:login");
      $stmt->bindParam(':login', $login);
      $stmt->bindParam(':argent', $argent);
      $stmt->execute();
    }

    public function supprInventaire($login, $nom){
      spl_autoload_register(function ($className) { @include "$className.php"; });
      $bd = Database::getInstance();
      $id_joueur= $this->recupId($login);
      $id_objet = $this->recupIdObjet($nom);
      $objet= $this->recupObjet($id_joueur, $id_objet);
      if(($objet[0]['qte']-1) <= 0){
          $stmt = $bd->prepare("DELETE FROM POSSEDER WHERE id_joueur=:id_joueur AND id_objet=:id_objet");
      }
      else{
        $qte = $objet[0]['qte'] -1;
          $stmt = $bd->prepare("UPDATE POSSEDER SET qte=:qte WHERE id_joueur=:id_joueur AND id_objet=:id_objet");
          $stmt->bindParam(':qte', $qte);
      }
      $stmt->bindParam(':id_joueur', $id_joueur);
      $stmt->bindParam(':id_objet', $id_objet);
      $stmt->execute();
    }
  }
?>
